package com.example.ttn.weather.request;

import com.example.ttn.weather.BuildConfig;
import com.google.gson.Gson;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiClient {

    private static ApiInterface apiInterface = null;

    public static ApiInterface getApi() {
        if (apiInterface == null) {

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient.Builder client = new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(new LanguageInterceptor())
                    .addInterceptor(new AuthenticationInterceptor());

            RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io());
            Retrofit api = new Retrofit.Builder()
                    .baseUrl("https://dataservice.accuweather.com/")
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .addCallAdapterFactory(rxAdapter)
                    .build();

            apiInterface = api.create(ApiInterface.class);
        }
        return apiInterface;
    }

}
