package com.example.ttn.weather;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.example.ttn.weather.utils.GoogleApiListener;
import com.example.ttn.weather.utils.GoogleClientInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import static com.example.ttn.weather.LocationService.FASTEST_LOCATION_INTERVAL;
import static com.example.ttn.weather.LocationService.LOCATION_INTERVAL;

/**
 * Contain google services api initialization
 * <p>
 * Created by bsh on 09.07.16.
 * All rights reserved.
 */
public abstract class GoogleServicesActivity extends AppCompatActivity implements GoogleApiListener {

    public static final String ACTION_PROVIDERS_CHANGED = "android.location.PROVIDERS_CHANGED";
    public static final String ACTION_LOCATION_NOT_AVAILABLE = BuildConfig.APPLICATION_ID + ".location_no_available";

    /**
     * Request code for check GPS settings
     */
    private static final int REQUEST_CHECK_SETTINGS = 616;

    /**
     * Request code for check permissions
     */
    private static final int REQUEST_PERMISSIONS = 617;

    private boolean isLocationReady;

    private boolean canRequestLocation = true;
    private boolean isGpsStateReceived;

    private BroadcastReceiver gpsStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isGpsStateReceived = true;
            if (intent.getAction() != null && intent.getAction().matches(ACTION_PROVIDERS_CHANGED)) {
                checkPermissions();
            }
        }
    };

    protected void onLocationStateChanged(boolean isReady) {
        isLocationReady = isReady;
        if (!isReady) {

        }
    }

    private static void sendLocationNotAvailable(@NonNull Context context) {
        Intent intent = new Intent();
        intent.setAction(ACTION_LOCATION_NOT_AVAILABLE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * Here you can start location work
     */
    protected void onLocationReady() {
        onLocationStateChanged(true);

        GoogleApiClient client = ((GoogleClientInterface) getApplication()).getApiClient();
        if (!client.isConnected() && !client.isConnecting()) {
            client.connect();
        } else {
            WeatherApplication.getInstance().getLocationService().start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PROVIDERS_CHANGED);

        registerReceiver(gpsStateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(gpsStateReceiver);
    }

    @SuppressWarnings("deprecation")
    protected void checkPermissions() {
        // if marshmallow or later and coarse and fine location not granted
        // request permissions from user
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // simple permission request
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS);

        } else {
            createLocationRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createLocationRequest();
            } else {
                showGpsNotGrantedAlert();
            }
        }
    }

    private void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_LOCATION_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        ((GoogleClientInterface) getApplication()).getApiClient(), builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result1) {
                Status status = result1.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // gps already enabled
                        onLocationReady();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // show explanation dialog
                        try {
                            status.startResolutionForResult(
                                    GoogleServicesActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // nothing
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // oohhh :(
                        showGpsNotGrantedAlert();
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                onLocationReady();
            } else {
                showGpsNotGrantedAlert();
            }
        }
    }

    private void showGpsNotGrantedAlert() {
        canRequestLocation = false;
        onLocationStateChanged(false);
        new AlertDialog.Builder(this)
                .setMessage(R.string.alert_permission_location_not_granted)
                .setPositiveButton(android.R.string.ok, null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        sendLocationNotAvailable(GoogleServicesActivity.this);
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (isLocationReady) {
            WeatherApplication.getInstance().getLocationService().start();
        } else if (canRequestLocation) {
            checkPermissions();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // nothing
        onLocationStateChanged(false);
    }

    boolean mResolvingError;

    private static final int REQUEST_RESOLVE_ERROR = 332;

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        if (!mResolvingError) {
            if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    WeatherApplication.getInstance().getApiClient().connect();
                }
            } else {
                if (GoogleApiAvailability.getInstance().isUserResolvableError(result.getErrorCode())) {
                    Dialog d = GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(),
                            REQUEST_RESOLVE_ERROR, new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    finish();
                                }
                            });
                    d.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });
                    d.show();
                } else {
                    showError();
                }
                mResolvingError = true;
            }
        }  // Already attempting to resolve an error.

    }

    private void showError() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.alert_play_services)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                })
                .show();
    }
}
