
package com.example.ttn.weather.model;

import java.util.List;

import com.example.ttn.weather.model.forecast.DailyForecast;
import com.example.ttn.weather.model.forecast.Headline;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForecastResult {

    @SerializedName("Headline")
    @Expose
    private Headline headline;
    @SerializedName("DailyForecasts")
    @Expose
    private List<DailyForecast> dailyForecasts = null;

    public Headline getHeadline() {
        return headline;
    }

    public void setHeadline(Headline headline) {
        this.headline = headline;
    }

    public List<DailyForecast> getDailyForecasts() {
        return dailyForecasts;
    }

    public void setDailyForecasts(List<DailyForecast> dailyForecasts) {
        this.dailyForecasts = dailyForecasts;
    }

}
