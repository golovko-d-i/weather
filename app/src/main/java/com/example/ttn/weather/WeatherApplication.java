package com.example.ttn.weather;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.ttn.weather.utils.GoogleApiListener;
import com.example.ttn.weather.utils.GoogleClientInterface;
import com.example.ttn.weather.utils.SimpleActivityLifecycleCallbacks;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class WeatherApplication extends Application implements GoogleClientInterface, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static WeatherApplication sInstance;
    private LocationService locationService = new LocationService();
    protected GoogleApiClient mGoogleApiClient;

    private int activitiesCount = 0;

    protected final ArrayList<GoogleApiListener> googleConnectionListeners = new ArrayList<>();

    public static WeatherApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        registerActivityLifecycleCallbacks(new SimpleActivityLifecycleCallbacks() {

            @Override
            public void onActivityResumed(Activity activity) {
                super.onActivityResumed(activity);
                activitiesCount++;

                if (activity instanceof GoogleApiListener) {

                    if (!mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.connect();
                    }

                    googleConnectionListeners.add((GoogleApiListener) activity);
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                super.onActivityPaused(activity);
                if (--activitiesCount <= 0) {
                    activitiesCount = 0;
                    startServicesCountdownIfPossible();
                }
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void startServicesCountdownIfPossible() {
        if (activitiesCount > 0) {
            return;
        }
        getLocationService().stop();
    }

    public LocationService getLocationService() {
        return locationService;
    }


    @Override
    public GoogleApiClient getApiClient() {
        return mGoogleApiClient;
    }

    @Override
    public void registerListener(GoogleApiListener apiListener) {
        if (!googleConnectionListeners.contains(apiListener)) {
            googleConnectionListeners.add(apiListener);
        }
        if (!googleConnectionListeners.isEmpty() && mGoogleApiClient != null && !(mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting())) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void unregisterListener(GoogleApiListener apiListener) {
        googleConnectionListeners.remove(apiListener);
        if (googleConnectionListeners.isEmpty() && mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        for (GoogleApiListener listener : googleConnectionListeners) {
            listener.onConnected(bundle);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        for (GoogleApiListener listener : googleConnectionListeners) {
            listener.onConnectionSuspended(i);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        for (GoogleApiListener listener : googleConnectionListeners) {
            listener.onConnectionFailed(connectionResult);
        }
    }
}
