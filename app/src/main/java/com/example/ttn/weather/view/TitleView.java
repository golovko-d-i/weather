package com.example.ttn.weather.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ttn.weather.R;
import com.example.ttn.weather.model.CurrentCondition;
import com.example.ttn.weather.model.LocationResult;

public final class TitleView extends LinearLayout {

    private TextView title, sub;

    public TitleView(Context context) {
        super(context);
        init();
    }

    public TitleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TitleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        LayoutInflater.from(getContext()).inflate(R.layout.view_title, this, true);
        title = findViewById(R.id.title);
        sub = findViewById(R.id.current_situation);
    }

    @SuppressLint("SetTextI18n")
    public void setModel(LocationResult locationResult, CurrentCondition currentCondition) {
        title.setText(locationResult.getLocalizedName());
        sub.setText(currentCondition.getWeatherText() + ", "
                + currentCondition.getTemperature().getMetric().getValue()
                + getResources().getString(R.string.celsius));
    }
}
