/*
 * Copyright (c) 2017. bsh
 */

package com.example.ttn.weather.request;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Authorization okhttp interceptor. Adds authorization header to all requests if user is logged in
 * <p>
 * Created by bsh on 04.12.2017.
 */

final class AuthenticationInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        HttpUrl url = chain.request().url().newBuilder()
                .addQueryParameter("apikey", "6Pj9fkS6IDZWlaGjnntuqCoDaPFbxSPD")
                .build();
        Request request = chain.request().newBuilder()
                .url(url)
                .build();
        return chain.proceed(request);
    }
}
