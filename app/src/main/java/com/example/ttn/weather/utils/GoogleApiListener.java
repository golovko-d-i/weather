package com.example.ttn.weather.utils;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Merged GoogleApi listeners interface
 */
public interface GoogleApiListener extends GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
}
