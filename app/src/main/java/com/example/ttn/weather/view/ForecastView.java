package com.example.ttn.weather.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ttn.weather.R;
import com.example.ttn.weather.model.forecast.DailyForecast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public final class ForecastView extends LinearLayout {

    private TextView date, temp, daynight;
    private static final DateFormat FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
    private static final SimpleDateFormat PARSE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);

    public ForecastView(Context context) {
        super(context);
        init();
    }

    public ForecastView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ForecastView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        LayoutInflater.from(getContext()).inflate(R.layout.view_forecast, this, true);
        date = findViewById(R.id.date);
        temp = findViewById(R.id.temp);
        daynight = findViewById(R.id.day_night);
    }

    public void setModel(DailyForecast forecast) {
        try {
            date.setText(FORMAT.format(PARSE.parse(forecast.getDate())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        temp.setText(getResources().getString(R.string.temp,
                String.valueOf(forecast.getTemperature().getMinimum().getValue()),
                String.valueOf(forecast.getTemperature().getMaximum().getValue())));
        daynight.setText(getResources().getString(R.string.daynight,
                forecast.getDay().getIconPhrase().toLowerCase(),
                forecast.getNight().getIconPhrase().toLowerCase()));
    }
}
