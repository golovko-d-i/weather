package com.example.ttn.weather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateUtils;

import com.example.ttn.weather.utils.GoogleClientInterface;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import io.reactivex.annotations.NonNull;

public class LocationService implements LocationListener {

    public static final long LOCATION_INTERVAL = 10 * DateUtils.MINUTE_IN_MILLIS;
    public static final long FASTEST_LOCATION_INTERVAL = DateUtils.MINUTE_IN_MILLIS;

    private Location sLocation = null;
    private LocationRequest locationRequest = null;

    public void start() {
        if (sLocation != null) {
            sendLocation(sLocation);
        }
        Context context = WeatherApplication.getInstance();
        if (context == null) {
            return;
        }
        GoogleApiClient googleApiClient = ((GoogleClientInterface) context).getApiClient();

        // check permissions for marshmallow and newer
        if (googleApiClient == null || !googleApiClient.isConnected()
                || Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // check permission
            return;
        }

        // send last known location
        sendLocation(LocationServices.FusedLocationApi.getLastLocation(googleApiClient));
        if (locationRequest == null) {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(LOCATION_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_LOCATION_INTERVAL);

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    public void stop() {
        Context context = WeatherApplication.getInstance();
        if (context == null) {
            return;
        }
        GoogleApiClient googleApiClient = ((GoogleClientInterface) context).getApiClient();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    ((GoogleClientInterface) context).getApiClient(), this);
        }
        locationRequest = null;
    }

    private void sendLocation(Location location) {
        if (location != null) {
            LocationReceiver.sendLocation(WeatherApplication.getInstance(), location);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        sendLocation(location);
    }

    public static abstract class LocationReceiver extends BroadcastReceiver {

        public static final String ACTION_LOCATION_RECEIVED = "location_received";

        @Override
        public final void onReceive(Context context, Intent intent) {
            if (intent != null && ACTION_LOCATION_RECEIVED.equals(intent.getAction())) {
                Location location = intent.getParcelableExtra("location");
                if (location != null) {
                    onLocationReceived(location);
                }
            }
        }

        public abstract void onLocationReceived(@NonNull Location location);

        public static void sendLocation(@NonNull Context context, @NonNull Location location) {
            Intent intent = new Intent();
            intent.setAction(ACTION_LOCATION_RECEIVED);
            intent.putExtra("location", location);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

        public static void registerListener(@NonNull Context context, @NonNull LocationReceiver receiver) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_LOCATION_RECEIVED);
            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
        }

        public static void unregisterListener(@NonNull Context context, @NonNull LocationReceiver receiver) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
        }
    }

}
