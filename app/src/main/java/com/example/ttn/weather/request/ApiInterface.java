package com.example.ttn.weather.request;

import com.example.ttn.weather.model.CurrentCondition;
import com.example.ttn.weather.model.ForecastResult;
import com.example.ttn.weather.model.LocationResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("/locations/v1/cities/geoposition/search")
    Observable<LocationResult> getLocationCode(@Query(value = "q") String query);

    @GET("/forecasts/v1/daily/5day/{code}?metric=true")
    Observable<ForecastResult> get5DayForecast(@Path(value = "code") String key);

    @GET("/currentconditions/v1/{code}")
    Observable<List<CurrentCondition>> currentCondition(@Path(value = "code") String key);

}
