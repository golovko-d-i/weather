package com.example.ttn.weather.utils;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Simple interface for providing {@link GoogleApiClient}
 */
public interface GoogleClientInterface {

    /**
     * Gets api client.
     *
     * @return the api client
     */
    GoogleApiClient getApiClient();

    /**
     * Register listener for updates
     *
     * @param apiListener listener instance
     */
    void registerListener(GoogleApiListener apiListener);

    /**
     * Unregister listener from updates
     *
     * @param apiListener listener instance
     */
    void unregisterListener(GoogleApiListener apiListener);

}
