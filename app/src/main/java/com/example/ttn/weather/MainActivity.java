package com.example.ttn.weather;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ttn.weather.model.CurrentCondition;
import com.example.ttn.weather.model.ForecastResult;
import com.example.ttn.weather.model.LocationResult;
import com.example.ttn.weather.model.forecast.DailyForecast;
import com.example.ttn.weather.request.ApiClient;
import com.example.ttn.weather.view.ForecastView;
import com.example.ttn.weather.view.TitleView;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends GoogleServicesActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Location location;
    private boolean request = true;
    private LocationResult locationResult;
    private CurrentCondition currentCondition;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private WeatherAdapter adapter = new WeatherAdapter();

    private LocationService.LocationReceiver locationReceiver = new LocationService.LocationReceiver() {
        @Override
        public void onLocationReceived(Location location) {
            // TODO
            MainActivity.this.location = location;
            if (request) {
                request = false;
                requestLocationCode();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        if (location != null) {
            requestLocationCode();
        }
    }

    private void requestLocationCode() {
        ApiClient.getApi().getLocationCode(location.getLatitude() + "," + location.getLongitude())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<LocationResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LocationResult locationResult) {
                        MainActivity.this.locationResult = locationResult;
                        requestCurrentCondition(locationResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(MainActivity.this, R.string.alert_cant_locate, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void requestCurrentCondition(LocationResult result) {
        ApiClient.getApi().currentCondition(result.getKey())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CurrentCondition>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<CurrentCondition> currentConditions) {
                        currentCondition = currentConditions.get(0);
                        requestForecast(locationResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(MainActivity.this, R.string.alert_cant_get_forecast, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void requestForecast(LocationResult result) {
        ApiClient.getApi().get5DayForecast(result.getKey())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ForecastResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ForecastResult forecastResult) {
                        adapter.setData(locationResult, currentCondition, forecastResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(MainActivity.this, R.string.alert_cant_get_forecast, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationService.LocationReceiver.registerListener(this, locationReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationService.LocationReceiver.unregisterListener(this, locationReceiver);
    }

    private static class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Feed data;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            if (viewType == 0) {
                return new TitleHolder(new TitleView(viewGroup.getContext()));
            } else {
                return new ForecastHolder(new ForecastView(viewGroup.getContext()));
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int pos) {
            if (getItemViewType(pos) == 0) {
                ((TitleHolder) viewHolder).view.setModel(data.locationResult, data.currentCondition);
            } else {
                ((ForecastHolder) viewHolder).view.setModel(data.getForecastItem(pos));
            }
        }

        @Override
        public int getItemViewType(int position) {
            return data != null ? data.getItemType(position) : 0;
        }

        @Override
        public int getItemCount() {
            return data != null ? data.size() : 0;
        }

        public void setData(LocationResult locationResult, CurrentCondition currentCondition, ForecastResult forecastResult) {
            this.data = new Feed(locationResult, currentCondition, forecastResult);
            notifyDataSetChanged();
        }
    }

    private static class TitleHolder extends RecyclerView.ViewHolder {

        public TitleView view;

        public TitleHolder(@NonNull TitleView itemView) {
            super(itemView);
            view = itemView;
        }
    }

    private static class ForecastHolder extends RecyclerView.ViewHolder {

        public ForecastView view;

        public ForecastHolder(@NonNull ForecastView itemView) {
            super(itemView);
            view = itemView;
        }
    }

    private static class Feed {

        public final LocationResult locationResult;
        public final CurrentCondition currentCondition;
        public final ForecastResult forecastResult;

        Feed(LocationResult locationResult, CurrentCondition currentCondition, ForecastResult forecastResult) {
            this.locationResult = locationResult;
            this.currentCondition = currentCondition;
            this.forecastResult = forecastResult;
        }

        public int getItemType(int pos) {
            if (pos == 0) {
                return 0;
            } else {
                return 1;
            }
        }

        public DailyForecast getForecastItem(int pos) {
            return forecastResult.getDailyForecasts().get(pos - 1);
        }

        public int size() {
            return forecastResult.getDailyForecasts().size() + 1;
        }

    }
}
